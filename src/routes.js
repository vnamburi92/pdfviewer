import React from 'react';
import { Route } from 'react-router';
import { App, PdfViewer } from './components';


export default (
    <Route path="/" component={App}>
      <Route path=":name" component={PdfViewer} />
    </Route>
);
