import React, { Component } from 'react';
import { Link, browserHistory } from 'react-router';
import { PDF, Viewer } from '../';
import './PdfViewer.css';


class PdfViewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: parseInt(this.props.location.query.page, 10),
      pages: 1,
      file: `${this.props.params.name}.pdf`,
      scale: 1,
      search: '',
    };
  }

  componentWillMount = () => {
    if (!this.props.location.query.page) {
      browserHistory.push(`/${this.props.params.name}?page=1`);
    }
  }

  componentDidMount = () => {
    window.addEventListener('resize', this.resize);
  }

  componentWillUnmount = () => {
    window.removeEventListener('resize');
  }

  resize = () => {
    if (window.innerWidth <= 769) {
      this.setState({ scale: 1.3 });
    } else {
      this.setState({ scale: 1.5 });
    }
  }

  componentWillReceiveProps = (nextProps) => {
    if (nextProps.params.name !== this.props.params.name) {
      browserHistory.push(`/${nextProps.params.name}?page=1`);
    }
  }

  render() {
    const { location, params } = this.props;
    const currentPage = parseInt(location.query.page, 10);
    const nextPage = currentPage + 1;
    const prevPage = currentPage - 1;
    const pdfFile = `/${this.props.params.name}.pdf`;
    return (
      <div className="pdf-wrapper row">
        <div className="pdf-header">
          <Link to={`/${params.name}?page=${prevPage}`} className="btn btn-default prev-button">Previous page</Link>
          <Link to={`/${params.name}?page=${nextPage}`} className="btn btn-default next-button" >Next page</Link>
          <div className="pdf-pageNumber">
            <a> Page: {currentPage} </a>
          </div>
        </div>
        <div className="pdf-body">
          <PDF src={pdfFile} currentPage={currentPage} search={this.state.search} >
            <Viewer currentPage={currentPage} location={location} />
          </PDF>
        </div>
      </div>
    );
  }
}

export default PdfViewer;
