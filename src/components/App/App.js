import React, { Component } from 'react';
import cx from 'classnames';
import { List, SubList } from '../';
import Json from '../../../public/test.json';
import './App.css';


class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showMobileNav: false,
    };
  }

  render() {
    const { showMobileNav } = this.state;
    return (
      <div className="App-body">
      <div className="mobile-header visible-sm visible-xs">
        <div className="">
          <button type="button" className="navbar-toggle collapsed" onClick={() => this.setState({ showMobileNav: !this.state.showMobileNav })}>
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
            <span className="icon-bar"></span>
          </button>
          <h4>
          Qantas Engineering Procedures
          </h4>
        </div>
      </div>
      <div className=" container-fluid">
        <div className={ cx('col-md-3 col-xs-12', { 'visible-lg visible-md': !showMobileNav, showMobileNav })} >
        <h4>Qantas Engineering Procedures Manual Table of Contents</h4>
          <div>
            <List list={Json.mainLinks} params={this.props.params} />
            <SubList params={this.props.params} />
          </div>
        </div>

        <div className="col-md-9 col-xs-12">
          {this.props.children}
        </div>
        </div>
      </div>
    );
  }
}

export default App;
