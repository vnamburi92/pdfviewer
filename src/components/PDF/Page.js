import React, { Component, PropTypes } from 'react';
import PDF from './PDF';
import './PDF.css';

class Page extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: 'N/A',
      page: null,
      width: 0,
      height: 0,
    };
  }

  static propTypes = {
    index: PropTypes.number.isRequired,
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.context.pdf !== nextContext.pdf || this.state.status !== nextState.status;
  }

  componentDidUpdate(nextProps, nextState, nextContext) {
    this._update(nextContext.pdf);
  }

  componentDidMount() {
    this._update(this.context.pdf);
  }

  _update(pdf) {
    if (pdf) {
      this._loadPage(pdf);
    } else {
      this.setState({ status: 'loading' });
    }
  }

  _loadPage(pdf) {
    if (this.state.status === 'rendering' || this.state.page != null) return;
    pdf.getPage(this.props.index).then(this._renderPage);
    this.setState({ status: 'rendering' });
  }

  _renderPage = (page) => {
      /* eslint-disable */
    const container = this.refs[`page-render-${this.props.index}`];

    const pdfPageView = new PDFJS.PDFPageView({
      container,
      id: this.props.index,
      scale: 1.0,
      defaultViewport: page.getViewport(1.0),
      // We can enable text/annotations layers, if needed
      textLayerFactory: new PDFJS.DefaultTextLayerFactory(),
      annotationLayerFactory: new PDFJS.DefaultAnnotationLayerFactory()
    });

    // Associates the actual page with the view, and drawing it
    pdfPageView.setPdfPage(page);
    pdfPageView.draw();
  }

  render() {
    // const { width, height, status } = this.state;
    const { status } = this.state;
    return (
      <div className={'pdf-page'} ref={`page-render-${this.props.index}`} />
    );
  }
}

Page.contextTypes = PDF.childContextTypes;

export default Page;
