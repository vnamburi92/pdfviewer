import React, { Component, PropTypes } from 'react';
import './PDF.css';
// const PDF_URL = 'https://mozilla.github.io/pdf.js/web/compressed.tracemonkey-pldi-09.pdf'

let pdfViewer = '';
let container = '';
let pdfFindController = '';

class PDFComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pdf: null,
      scale: 1.2,
    };
  }

  static propTypes = {
    src: PropTypes.string.isRequired,
  }

  static childContextTypes = {
    pdf: PropTypes.object,
    scale: PropTypes.number,
  }

  getChildContext() {
    return {
      pdf: this.state.pdf,
      scale: this.state.scale,
    };
  }

  componentDidMount() {
    this.renderPDF(this.props.src);
  }

  shouldComponentUpdate(nextProps) {
    return nextProps.src !== this.props.src;
  }

  componentWillReceiveProps(nextProps) {
    if (this.props.src !== nextProps.src) {
      this.renderPDF(nextProps.src);
    } else if (this.props.currentPage !== nextProps.currentPage) {
      pdfViewer.currentPageNumber = nextProps.currentPage;
    } else if (this.props.search !== nextProps.search) {
      this.findController(nextProps.search);
    }
  }
  /* eslint-disable */
  renderPDF = (src) => {
    container = this.refs.viewerContainer;

    pdfViewer = new PDFJS.PDFViewer({
      container,
    });

    this.findController();

    pdfViewer.currentPageNumber = this.props.currentPage;

    PDFJS.getDocument(src)
    .then(pdf => pdfViewer.setDocument(pdf));
  }
  /* eslint-enable */
  /* eslint-disable */
  findController = (query = '') => {
    console.log('being hit')
    pdfFindController = new PDFJS.PDFFindController({
      pdfViewer,
    });

    pdfViewer.setFindController(pdfFindController);

    container.addEventListener('pagesinit', function () {
      // We can use pdfViewer now, e.g. let's change default scale.
      pdfViewer.currentScaleValue = 'page-width';
      pdfFindController.executeCommand('find', {query});
      pdfFindController.extractText();
    });
  }

  /* eslint-enable */
  render() {
    return (
    <div className="pdf-context">
      <div id="viewerContainer" ref="viewerContainer">
        <div id="viewer" className="pdfViewer"></div>
      </div>
    </div>
    );
  }
}

export default PDFComponent;
