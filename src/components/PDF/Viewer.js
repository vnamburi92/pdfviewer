import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import _ from 'lodash';
import PDF from './PDF';
import Page from './Page';

class Viewer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      currentPage: this.props.currentPage,
      scrollPosition: 0,
    };
  }

  componentDidMount() {
    const { currentPage } = this.state;
    this.setState({ scrollPosition: this.changeScroll(currentPage) });
    this.viewer.scrollTop = this.changeScroll(currentPage);
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.currentPage > this.props.currentPage) {
      this.viewer.scrollTop = (1112 * this.props.currentPage);
    } else if (nextProps.currentPage < this.props.currentPage) {
      this.viewer.scrollTop = (1112 * (nextProps.currentPage - 1));
    }
  }

  shouldComponentUpdate(nextProps, nextState, nextContext) {
    return this.props.currentPage !== nextProps.currentPage || this.context.pdf !== nextContext.pdf;
  }

  changeScroll = (currentPage) => {
    const viewer = this.viewer.getBoundingClientRect();
    const current = ReactDOM.findDOMNode(this[`page-${currentPage}`]).getBoundingClientRect();
    return current.top - viewer.top;
  }

  render() {
    /* eslint-disable */
    const { pdf } = this.context;
    const { currentPage } = this.props;
    const numPages = pdf ? pdf.pdfInfo.numPages : 0;
    const fingerprint = pdf ? pdf.pdfInfo.fingerprint : 'none';
    const pages = _.range(numPages)

    return (
      <div
      className="pdf-viewer"
      ref={(ref) => { this.viewer = ref; }}
      >
        {_.map([0, 1, 2, 3, 4, 5], (v, i) => <Page
          index={i + 1}
          ref={(ref) => { this[`page-${i + 1}`] = ref; }}
          key={`${fingerprint}-${i + 1}`}
        />)}
      </div>
    );
  }
}

Viewer.contextTypes = PDF.childContextTypes;

export default Viewer;
