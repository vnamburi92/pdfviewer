import React, { Component } from 'react';
import './SubList.css';
import Json from '../../../public/test.json';
import { DropDown } from '../';

class SubList extends Component {

  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }


  subLinks = () => Json.subLinks.map((item) =>
  <DropDown heading={item.heading} mainLinks={item.mainLinks} key={item.heading} params={this.props.params} />,
  )
  ;

  render() {
    return (
        <div className="sub-list">
          <ul>
            { this.subLinks() }
          </ul>
        </div>
    );
  }
}

export default SubList;
