import React, { Component } from 'react';
import { Link } from 'react-router';
import './List.css';

class List extends Component {


  links = () => this.props.list.map((item) => {
    const currentPDF = this.props.params.name;
    return (<li key={item.title}>
      { currentPDF !== item.link ? <Link to={`/${item.link}`}>{item.title}</Link> : <span>{item.title}</span>}
    </li>);
  });

  render() {
    return (
      <div className="list">
        <ul >
          { this.links() }
        </ul>
      </div>
    );
  }
}

export default List;
