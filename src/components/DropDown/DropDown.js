import React, { Component } from 'react';
import { List } from '../';

class DropDown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isOpen: false,
    };
  }
  render() {
    const { heading, mainLinks, params } = this.props;
    return (
      <li key={heading}>
      <a onClick={() => this.setState({ isOpen: !this.state.isOpen })} > {heading} </a>

      { this.state.isOpen && <List list={ mainLinks } params={params}/ >}

      </li>
    );
  }
}

export default DropDown;
